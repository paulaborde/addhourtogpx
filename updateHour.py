# -*- encoding: utf-8 -*-
# Paul Laborde update hour for GPX files
# Python 3.x code

# libraries
from datetime import datetime, timedelta

# inputs
input_file = 'activity_10483578929.gpx'
minutes_delay = 30

print("°° Start Update hour for GPX file °°")

output_file = '%s_updated.gpx' % input_file.split('.')[0]
newfile = open(output_file, 'a')

myfile = open(input_file)
for l in myfile :
    if 'time' in l:
        str_datetime = l.replace('<time>','').replace('</time>', '').replace('\n','').replace(' ','')
        tmp_dt = datetime.strptime(str_datetime, '%Y-%m-%dT%H:%M:%S.%fZ')
        tmp_dt += timedelta(minutes = minutes_delay)

        temp_line = '<time>%s</time>' % tmp_dt.strftime('%Y-%m-%dT%H:%M:%S.%f%Z')
        newfile.write(temp_line)

    else:
        newfile.write(l)

myfile.close()

print("New result file is generated : %s" % output_file)